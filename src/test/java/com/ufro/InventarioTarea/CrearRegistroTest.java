package com.ufro.InventarioTarea;

import com.ufro.InventarioTarea.models.Producto;
import com.ufro.InventarioTarea.repository.RegistroRepository;
import com.ufro.InventarioTarea.service.ProductoService;
import com.ufro.InventarioTarea.service.RegistroService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CrearRegistroTest {

    Producto producto1;
    Producto producto2;

    boolean flag =false;
    @Autowired
    private ProductoService productoService;
    @Autowired
    private RegistroService registroService;

    @Autowired
    private RegistroRepository registroRepository;

    @BeforeAll
    static void all() {
        System.out.println("BEFORE ALL");
    }

    @BeforeEach
    void setup() {
        long id1 =1;
        long id2 =2;
        producto1= new Producto(id1,"Monster","bebida energetica",10,null,null);
        producto2 = new Producto(id2,"Triton1111111111111111111111111111","galletas",0,null,null);
        productoService.save(producto1);
        productoService.save(producto2);
        registroService.añadirProducto(producto1);
        System.out.println("BEFORE EACH");

    }
    @AfterEach
    void vaciar(){
        producto1=null;
        producto2=null;
        this.flag= false;
    }
    @Test
    @DisplayName("Generar Registro")
    public void validarRegistro(){
        long id =12;
        System.out.println(registroService.get(id));
        Assertions.assertNotNull(registroService.get(id));
    }

    @Test
    @DisplayName(("Validar Accion"))

    public void validarAccion(){
        long id =13;
        registroService.editarProducto(producto1);
        registroService.get(id);
        Assertions.assertEquals("Se edito un producto",registroService.get(id).getAccion());
    }

}
