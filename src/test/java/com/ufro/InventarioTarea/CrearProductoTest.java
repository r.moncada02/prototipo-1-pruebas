package com.ufro.InventarioTarea;

import com.ufro.InventarioTarea.models.Producto;
import com.ufro.InventarioTarea.repository.ProductoRepository;
import com.ufro.InventarioTarea.repository.RegistroRepository;
import com.ufro.InventarioTarea.service.ProductoService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootTest
@DisplayName("Pruebas crear producto")
public class CrearProductoTest {

    Producto producto1;
    Producto producto2;

    boolean flag =false;
    @Autowired
    private ProductoService productoService;


    @BeforeAll
    static void all() {
        System.out.println("BEFORE ALL");
    }
    @BeforeEach
    void setup() {
        long id1 =1;
        long id2 =2;
        producto1= new Producto(id1,"Monster","bebida energetica",10,null,null);
        producto2 = new Producto(id2,"Triton1111111111111111111111111111","galletas",0,null,null);
        productoService.save(producto1);
        productoService.save(producto2);
        System.out.println("BEFORE EACH");

    }
    @AfterEach
    void vaciar(){
        producto1=null;
        producto2=null;
        this.flag= false;
    }

    @Test
    @DisplayName("Crear producto")
    @ParameterizedTest
    @ValueSource(ints = {1,3})
    public void validarCreacion(long id){

        Assertions.assertNotNull(productoService.get(id));
    }
    @Test
    @DisplayName("Largo de nombre")
    @ParameterizedTest
    @ValueSource(ints = {1,2})
    public void validarLargoNombre(long id){


        if(productoService.get(id).getNombre().length()<20){
            System.out.printf("Largo de nombre Correcto");
            flag=true;
        }else {
            throw new IllegalArgumentException("El nombre debe tener como maximo 20 caracteres");
        }
        Assertions.assertEquals(true,flag);

    }
    @Test
    @DisplayName("Cantidad mayor a 0")
    @ParameterizedTest
    @ValueSource(ints = {1,2})
    public void validarCantidad(long id){
        if(productoService.get(id).getCantidad()>0){
            System.out.println("Cantidad correcta");
            flag=true;

        }else {
            throw new IllegalArgumentException("La cantidad debe ser mayor a 0");
        }
        Assertions.assertEquals(true,flag);
    }




}
