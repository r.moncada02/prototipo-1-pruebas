package com.ufro.InventarioTarea;

import com.ufro.InventarioTarea.models.Producto;
import com.ufro.InventarioTarea.service.ProductoService;
import com.ufro.InventarioTarea.service.RegistroService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@DisplayName("Cambiar cantidades")
public class CambiarCantidadTest {
    Producto producto1;
    Producto producto2;

    boolean flag =false;
    @Autowired
    private ProductoService productoService;




    @BeforeAll
    static void all() {
        System.out.println("BEFORE ALL");


    }

    @BeforeEach
    void setup() {
        long id1 =1;
        long id2 =2;
        producto1= new Producto(id1,"Monster","bebida energetica",10,null,null);
        producto2 = new Producto(id2,"Triton1111111111111111111111111111","galletas",0,null,null);
        productoService.save(producto1);
        productoService.save(producto2);
        System.out.println("BEFORE EACH");

    }
    @AfterEach
    void vaciar(){
        producto1=null;
        producto2=null;
        this.flag= false;
    }

    @Test
    @DisplayName("Cambio de cantidad")
    public void validarCambio(){
        long id =1;
        Producto producto = productoService.get(id);
        int cantidad1= producto.getCantidad();
        System.out.println(producto.getCantidad());
        producto.setCantidad(50);
        productoService.save(producto);
        System.out.println(producto.getCantidad());
        int cantidad2 = producto.getCantidad();
        Assertions.assertNotEquals(cantidad1,cantidad2);

    }
}
