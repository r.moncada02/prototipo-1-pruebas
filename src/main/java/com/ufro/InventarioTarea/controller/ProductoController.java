package com.ufro.InventarioTarea.controller;

import com.ufro.InventarioTarea.models.Producto;
import com.ufro.InventarioTarea.service.ProductoService;
import com.ufro.InventarioTarea.service.RegistroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Esta clase sirve para llamar las rutas relacionadas la tabla de productos
 * @autor Rodrigo Moncada
 *
 */
@Controller
public class ProductoController {


    @Autowired
    private ProductoService productoService;
    @Autowired
    private RegistroService registroService;

    /**
     * Este metodo llama a la ventana principal donde se listan los productos
     * @param modelo contiene los atributos de producto
     * @return devuelve el html index
     */
    @RequestMapping("/")
    public String verPaginaDeInicio(Model modelo){
        List<Producto> listaProductos = productoService.listAll();
        modelo.addAttribute("listaProductos",listaProductos);
        return "index";
    }

    /**
     * Este metodo crea un nuevo producto
     * @param modelo contiene los atributos de producto
     * @return devuelve el html nuevo_producto
     */
    @RequestMapping("/nuevo")
    public String mostrarFormularioDeRegistroProducto(Model modelo){
        Producto producto = new Producto();
        modelo.addAttribute("producto",producto);
        return "nuevo_producto";
    }

    /**
     * Este metodo almacena el producto n la base de datos
     * @param producto contiene los atributos de producto
     * @return redirecciona a la ventana principal
     */
    @RequestMapping(value="/guardar",method = RequestMethod.POST)
    public String guardarProducto(@ModelAttribute("producto")Producto producto){
        productoService.save(producto);
        registroService.añadirProducto(producto);
        return "redirect:/";
    }

    /**
     * Este metodo permite editar los atributos de un producto en la base de datos
     * @param id es la id del producto a editar
     * @return devuelve el modelo de producto
     */
    @RequestMapping("/editar/{id}")
    public ModelAndView mostrarFormularioDeEditarProducto(@PathVariable(name="id")Long id){
        ModelAndView modelo = new ModelAndView("editar_producto");
        Producto producto = productoService.get(id);
        modelo.addObject("producto",producto);
        registroService.editarProducto(producto);
        return modelo;


    }

    /**
     * Este metodo permite eliminar un producto de la base de datos
     * @param id es la id del producto a eliminar
     * @return redirecciona a la pagina principal
     */
    @RequestMapping("/eliminar/{id}")
    public String eliiminarProducto(@PathVariable(name="id")Long id){
        Producto producto = productoService.get(id);
        registroService.eliminarProducto(producto);
        productoService.delete(id);

        return "redirect:/";
    }


}
