package com.ufro.InventarioTarea.controller;

import com.ufro.InventarioTarea.models.Registro;
import com.ufro.InventarioTarea.service.RegistroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Esta clase llama las rutas relacionadas con registro
 * @author Rodrigo Moncada
 */
@Controller
public class RegistroController {
    @Autowired
    private RegistroService registroService;

    @RequestMapping("/registros")
    public String verRegistros(Model modelo){
        List<Registro> listaRegistro = registroService.listAll();
        modelo.addAttribute("listaRegistro",listaRegistro);
        return "lista_registro";
    }
}
