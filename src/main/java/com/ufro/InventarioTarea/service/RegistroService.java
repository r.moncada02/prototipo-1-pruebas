package com.ufro.InventarioTarea.service;

import com.ufro.InventarioTarea.models.Producto;
import com.ufro.InventarioTarea.models.Registro;
import com.ufro.InventarioTarea.repository.RegistroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

/**
 * Esta clase es el trabaja con la logica del modelo registro
 * @author Rodrigo Moncada
 */
@Service
public class RegistroService {
    @Autowired
    private RegistroRepository registroRepository;

    public List<Registro> listAll(){
        return registroRepository.findAll();
    }

    public Registro get(Long id){
        return registroRepository.findById(id).get();
    }

    public void añadirProducto(Producto producto){
        Registro registro=new Registro();
        registro.setProducto(producto);
        registro.setFecha(LocalDate.now());
        String accion ="Se añadio un nuevo producto";
        registro.setCantidad("+"+producto.getCantidad());
        registro.setAccion(accion);
        registroRepository.save(registro);
    }
    public void editarProducto(Producto producto){
        Registro registro=new Registro();
        registro.setProducto(producto);
        registro.setFecha(LocalDate.now());
        String accion ="Se edito un producto";
        registro.setCantidad("+"+producto.getCantidad());
        registro.setAccion(accion);
        registroRepository.save(registro);
    }

    public void eliminarProducto(Producto producto){
        Registro registro=new Registro();
        registro.setProducto(producto);
        registro.setFecha(LocalDate.now());
        String accion ="Se elimino un producto";
        registro.setAccion(accion);
        registroRepository.save(registro);
    }

    @Controller
    public static class UsuarioController {
    }
}
