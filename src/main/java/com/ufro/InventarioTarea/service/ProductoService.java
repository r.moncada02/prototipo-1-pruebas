package com.ufro.InventarioTarea.service;

import com.ufro.InventarioTarea.models.Producto;
import com.ufro.InventarioTarea.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Esta clase es el trabaja con la logica del modelo producto
 * @author Rodrigo Moncada
 */
@Service
public class ProductoService {
    @Autowired
    private ProductoRepository productoRepository;
    public List<Producto> listAll(){
        return productoRepository.findAll();
    }
    public void save(Producto producto){
        productoRepository.save(producto);
    }
    public Producto get(Long id){
        return productoRepository.findById(id).get();
    }
    public void delete(Long id){
        productoRepository.deleteById(id);
    }

    public Producto almacenar(Producto producto){
        productoRepository.save(producto);
                return producto;
    }

}
