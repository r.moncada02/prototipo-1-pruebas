package com.ufro.InventarioTarea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventarioTareaApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventarioTareaApplication.class, args);
	}

}
