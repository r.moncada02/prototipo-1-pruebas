package com.ufro.InventarioTarea.repository;

import com.ufro.InventarioTarea.models.Registro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistroRepository extends JpaRepository<Registro,Long> {
}
