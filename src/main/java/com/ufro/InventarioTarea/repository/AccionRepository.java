package com.ufro.InventarioTarea.repository;

import com.ufro.InventarioTarea.models.Accion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccionRepository extends JpaRepository<Accion,Long> {
}
