package com.ufro.InventarioTarea.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
/**
 * Esta clase es el modelo de producto
 * @author Rodrigo Moncada
 */
@Entity

public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false,length = 60)
    private String nombre;
    @Column(nullable = false,length = 60)
    private String descripcion;
    @Column(nullable = false,length = 60)
    private int cantidad;



    @OneToMany(mappedBy = "producto")
    @Column(nullable = false,length = 60)
    private List<Registro> registros;

    @ManyToOne
    private Usuario usuario;

    public Producto(long id, String nombre, String descripcion, int cantidad,List<Registro> registros, Usuario usuario) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.registros = registros;
        this.usuario = usuario;
    }

    public Producto(){
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }



    public List<Registro> getRegistros() {
        return registros;
    }

    public void setRegistros(List<Registro> registros) {
        this.registros = registros;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}



