package com.ufro.InventarioTarea.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * Esta clase es el modelo de Accion
 * @author Rodrigo Moncada
 */
@Entity
@Data

public class Accion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nombre;

}
