package com.ufro.InventarioTarea.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
/**
 * Esta clase es el modelo de registro
 * @author Rodrigo Moncada
 */
@Entity
@Data
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    private String nombre;
    private String correo;
    private String clave;
    @OneToMany(mappedBy = "usuario")
    private List<Producto> productos;
    public Usuario(){

    }


}
