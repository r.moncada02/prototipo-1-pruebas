package com.ufro.InventarioTarea.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

/**
 * Esta clase es el modelo de registro
 * @author Rodrigo Moncada
 */
@Entity
@Data
public class Registro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private LocalDate fecha;
    private String cantidad;
    @ManyToOne
    private Usuario usuario;
    private String accion;
    @ManyToOne
    private Producto producto;


    public Registro(){

    }
    public String getFecha(){
        String parse = fecha.getDayOfMonth()+"/"+fecha.getMonthValue()+"/"+fecha.getYear();
        return parse;
    }

}
